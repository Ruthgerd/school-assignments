import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        // X1 PART 1
        System.out.print("\nEnter a number: ");

        makeNegative(scanner.nextInt());

        // X2
        System.out.print("\nEnter angle1: ");
        float angle1 = scanner.nextInt();
        System.out.print("Enter angle2: ");
        float angle2 = scanner.nextInt();

        thirdLength(angle1, angle2);
        // X3
        System.out.print("\nEnter employed bool: ");
        Boolean employed = scanner.nextBoolean();
        System.out.print("Enter vacation bool: ");
        Boolean vacation = scanner.nextBoolean();

        setAlarm(employed, vacation);

        // X4
        System.out.print("\nEnter n: ");
        int n = scanner.nextInt();
        System.out.print("Enter x: ");
        int x = scanner.nextInt();
        System.out.print("Enter y: ");
        int y = scanner.nextInt();

        isDivisible(n, x, y);

        // X5
        calculateRide(10, true, "Gold");

        // X1 PART 2
        interactiveQuiz(scanner);


        // X2
        System.out.print("\nEnter an integer: ");
        int userInteger = scanner.nextInt();
        isDivisible2(userInteger);

        // X3
        randomCard();
    
        // X4
        System.out.print("\nEnter the first 9 digits of an ISBN as integer: ");
        String ISBN = scanner.next();
        ISBNcheck(ISBN);

        // X5
        smallestN();

        // X6
        scanner.nextLine();
        System.out.print("\nInput integers seperated by a space: ");
        String numberInput = scanner.nextLine();
        posNegAvrg(numberInput);

        // X7
        System.out.print("\nEnter a positive integer under 256: ");
        int positiveInteger = scanner.nextInt();

        while (positiveInteger < 0 && positiveInteger < 256) {
            System.out.print("\nEnter a positive integer under 256: ");
            positiveInteger = scanner.nextInt();
        }
        toBinary(positiveInteger);

        // X8
        System.out.print("\nEnter a string: ");
        scanner.nextLine();

        String flipString = scanner.nextLine();
        reverseString(flipString);

        // X9
        System.out.print("\nGive a number to factorize: ");
        int factorize = scanner.nextInt();
        factorizeNumber(factorize);

        // X10
        System.out.print("\nEnter the amount of students: ");
        ArrayList<ArrayList<String>> studentData = new ArrayList<ArrayList<String>>();
        int students = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < students; i++) {
            ArrayList<String> temp = new ArrayList<String>();
            System.out.print("\nEnter the students' name: ");
            temp.add(scanner.nextLine());
            System.out.print("Enter the students' score: ");
            temp.add(Integer.toString(scanner.nextInt()));
            scanner.nextLine();
            studentData.add(temp);
        }

        bestStudent(studentData);
        
        // X11
        randomMonth();

        // X12
        System.out.print("\nInsert pyramid size: ");
        int pyramidSize = scanner.nextInt();
        drawPyramid(pyramidSize);
        
        
        scanner.close();

    }

    static void makeNegative(int number) {
        System.out.println("Negative version: " + Math.abs(number) * -1);
    }

    static void thirdLength(float angle1, float angle2) {
        System.out.println("The remaining angle is: " + (180f - (angle1 + angle2)) + " degrees " + angle1 + " " + angle2);
    }

    static void setAlarm(Boolean employed, Boolean vacation) {
        if (employed && !vacation) {
                System.out.println("BEEP BEEP its your alarm");
            } else {
                System.out.println("cointinue sleeping");
            }
    }

    static void isDivisible(int n, int x, int y) {
        if (n % x == 0 && n % y == 0) {
            System.out.println("true because " + n + " is divisible by " + x + " and " + y);
        }
        // add different output/check for partial true
    }
    static void calculateRide(int distance, Boolean charged, String membership) {
        int rideRate = 8;
        float finalCost = 0;
        if (distance < 5) {
            rideRate = 20;
        } else if(distance <= 15) {
            rideRate = 15;
        }
        finalCost = distance * rideRate;
        if (charged) {
            finalCost -= finalCost * 0.25f;
        }
        if (membership == "Silver") {
            finalCost = finalCost * 0.90f;
        } else if (membership == "Gold") {
            finalCost = finalCost * 0.80f;
        } else if (membership == "Platinum"){
            finalCost = finalCost * 0.65f;
        }
        System.out.println("\nFinal cost: " + finalCost + " SEK");
    }

    static void interactiveQuiz(Scanner scanner) {
        //Scanner scanner = new Scanner(System.in);
        System.out.print("\nAre you ready (Y/N)? ");
        String ready = scanner.next();
        if (ready.indexOf("Y") != -1) {
            System.out.println("\nQ1) What is the capital of Albania?\n  1) Sofia\n  2) Tirana\n  3) Budapest");
            int Q1 = scanner.nextInt();
            checkQuestion(Q1, 2);

            System.out.println("\nQ2) Can you store the value \"dog\" in variable of type int?\n  1) yes\n  2) no");
            int Q2 = scanner.nextInt();
            checkQuestion(Q2, 2);

            System.out.println("\nQ3) What is the result of 6+4/2?\n  1) 5\n  2) 8\n  3) 4");
            int Q3 = scanner.nextInt();
            checkQuestion(Q3, 2);
        }
    }
    static void checkQuestion(int inputAnswer, int expectedAnswer) {
        if (inputAnswer == expectedAnswer) {
            System.out.println("\nThats correct!");
        } else {
            System.out.println("\nThats incorrect!");
        }
    }

    static void isDivisible2(int userInteger) {
        Boolean devideBoth = (userInteger % 5 == 0 && userInteger % 6 == 0);
        Boolean devideEither = (userInteger % 5 == 0 || userInteger % 6 == 0);
        Boolean devideOne = (userInteger % 5 == 0 ^ userInteger % 6 == 0);

        System.out.println("Is " + userInteger + " divisible by 5 and 6? " + devideBoth);
        System.out.println("Is " + userInteger + " divisible by 5 or 6? " + devideEither);
        System.out.println("Is " + userInteger + " divisible by 5 or 6, but not both? " + devideOne);
    }

    static void randomCard() {
        ArrayList<String> cards = new ArrayList<String>();
        ArrayList<String> types = new ArrayList<String>();
        
        cards.add("Ace");
        for (int i = 0; i < 10; i++) {
            cards.add(Integer.toString(i + 1));
        }
        cards.add("Jack");
        cards.add("Queen");
        cards.add("King");

        types.add("Clubs");
        types.add("Diamonds");
        types.add("Hearts");
        types.add("Spades");


        int rand0 = (int) (Math.random() * 13);
        int rand1 = (int) (Math.random() * 4);

        System.out.println("\nThe card you picked is " + cards.get(rand0) + " of " + types.get(rand1));
    }

    static void ISBNcheck(String ISBN) {
        int totalISBN = 0;
        int checksum;
        for (int i = 0; i < 9; i++) {
            totalISBN += Integer.parseInt(ISBN.substring(i, i+1)) * (i + 1);
        }
        checksum = totalISBN % 11;
        String ISBNchecksum = Integer.toString(checksum);
        if (checksum > 9) {
            ISBNchecksum = "X";
        }
        System.out.println("The ISBN-10 number is " + ISBN + ISBNchecksum);
    }

    static void smallestN() {
        Boolean foundN = false;
        int n = 0;
        while(!foundN) {
            if (Math.pow(n, 2) > 12000) {
                foundN = true;
            } else {
                n++;
            }
        }
        System.out.println("\n1 The smallest n^2 > 12000 is " + n);
    }

    static void posNegAvrg(String numberInput) {
        String[] numbers = numberInput.split(" ");
        int positives = 0;
        int negatives = 0;
        float total = 0;
        float average = 0;
        for (int i = 0; i < numbers.length; i++) {
            int number = Integer.parseInt(numbers[i]);
            if (number > 0) {
                positives++;
            } else if (number < 0) {
                negatives++;
            }
            total += number;
        }
        average = total / (positives + negatives);
        System.out.println("The number of positives is " + positives);
        System.out.println("The number of negatives is " + negatives);
        System.out.println("The total is " + total);
        System.out.println("The average is " + average);
    }

    static void toBinary(int positiveInteger) {
        String binary = "";
        int powlevel = 7;
        double mod = positiveInteger;
        while(powlevel != 0) {
            double pow = Math.pow(2, powlevel);
            if (powlevel == 1) {
                pow = 1;
            }
            if((mod % pow) == mod) {
                binary += "0";
                powlevel--;
            } else {
                binary += "1";
                mod = mod % pow;
                powlevel--;
            }
        }
        System.out.println("The binary representation of " + positiveInteger + " is " + binary);
    }
    static void reverseString(String flipString) {
        String flopString = "";

        for (int i = flipString.length(); i > 0; i--) {
            flopString += flipString.substring(i- 1, i);
        }
        System.out.println("Flipped string: " + flopString);
    }

    static void factorizeNumber(int factorize) {
        ArrayList<Integer> sequence = new ArrayList<Integer>();
        int minimum = 2;
        System.out.print("Factorized: ");

        while(factorize != 1) {
            Boolean foundNextSequence = false;
            while(!foundNextSequence && factorize != 1){
                if (factorize % minimum == 0) {
                    factorize = factorize / minimum;
                    System.out.print(minimum+ ", ");
                    sequence.add(minimum);
                    foundNextSequence = true;
                } else {
                    minimum += 1;
                }
            }
        }
        System.out.println();
    }

    static void bestStudent(ArrayList<ArrayList<String>> studentData) {
        ArrayList<String> bestStudent = new ArrayList<String>();
        bestStudent.add("Noob");
        bestStudent.add("0");
        for (int i = 0; i < studentData.size(); i++) {
            if (Integer.parseInt(bestStudent.get(1)) < Integer.parseInt(studentData.get(i).get(1))) {
                bestStudent = studentData.get(i);
            }
        }

        System.out.println("The best student is: " + bestStudent.get(0) + " with the score of: " + bestStudent.get(1));
        
    }

    static void randomMonth() {
        ArrayList<String> months = new ArrayList<String>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");

        double randomMonth = Math.random();

        randomMonth = randomMonth * 12;
        System.out.println("\nYour random month is: " + months.get((int) randomMonth));
    }

    static void drawPyramid(int pyramidSize) {
        for(int i = 0; i < pyramidSize; i++) {
            for(int j = 1; j <= pyramidSize - i; j++){
                System.out.print("  ");
            }
            for(int l = i; l >=1; l--){
                System.out.print(l + " ");
            }            
            for(int k = 2; k <= i; k++){
                System.out.print(k + " ");
            }
            System.out.println(); 
        }
    }
}
