package com.ruthco.app;

public class App {
  
	public static void main(String[] args){
        Movie someMovie = new Movie("kimi no na wa", 2016, 112);

        for(int i = 0; i < 100; i++) {
            someMovie.like();
        }
        someMovie.dislike();

        System.out.println("Rating of: " + someMovie.getTitle() + " is: " + someMovie.getRating());

	}
}
