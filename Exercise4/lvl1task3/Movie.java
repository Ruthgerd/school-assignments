package com.ruthco.app;

public class Movie {
    private String title;
    private int releaseYear;
    private int length;
    private int rating;

    public Movie(String title, int releaseYear, int length) {
        this.title = title;
        this.releaseYear = releaseYear;
        this.length = length;
        this.rating = 0;
    }

    public void like() {
        this.rating++;
    }
    
    public void dislike() {
        if (rating != 0) {
          this.rating--;  
        } 
    }

    public String getTitle() {
        return this.title;
    }

    public int getRating() {
        return this.rating;
    }

    public int getLength() {
        return this.length;
    }

    public int getReleaseYear() {
        return this.releaseYear;
    }

}