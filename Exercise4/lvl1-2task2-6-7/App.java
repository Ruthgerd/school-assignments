package com.ruthco.app;

public class App {
  
	public static void main(String[] args){
        Account[] accounts = new Account[3];
        accounts[0] = new Account("Eve", 1000.39, "0");
        accounts[1] = new Account("John", 500000.00, "1");
        accounts[2] = new Account("Marry", 12540000.99, "2");

        for (Account account : accounts) {
            account.withdrawMoney(500);
            account.checkBalance();
        }

        for (Account account : accounts) {
            account.depositMoney(1500);
            account.checkBalance();
        }

	}

}
