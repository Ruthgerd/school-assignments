package com.ruthco.app;

public class Account {
    private String holderName;
    private String bankID;
    private double balance;

    public Account(String holderName, double balance, String bankID) {
        this.holderName = holderName;
        this.balance = balance;
        this.bankID = bankID;
    }

    public void checkBalance() {
        System.out.println("\nAccount holder: " + this.holderName + " | Remaining balance:" + this.balance);        
    }

    public void withdrawMoney(double amount) {
        this.balance -= Math.abs(amount); // no negative allowed
    }

    public void depositMoney(double amount) {
        if (this.balance > 0) {
            this.balance += Math.abs(amount);
        } else {
            this.balance += Math.abs(amount) * 0.9; // takes interest from negetive saldo
        }
    }

    public String getHolderName() {
        return this.holderName;
    }

    public String getID() {
        return this.bankID;
    }

    public double getBalance() {
        return this.balance;
    }
}