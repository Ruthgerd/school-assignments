package com.ruthco.app;

class Hero {
    private String name;
    private int hp;
    private int mana;

    public Hero(String name) {
        this.name = name;
        this.hp = 100;
        this.mana = 100;
    }

    public void changeHP(int amount) {
        int change = this.hp + amount;
        if (change < 0) {
            change = 0;
        }
        if(change > 100) {
            change = 100;
        }
        this.hp = change;
        if(this.hp <= 0) {
            System.out.println(this.name + ": I died!... But I regret nothing!");
        }
    }

    public void spendMana(int amount) {
        int change = this.mana - amount;
        if (change < 0) {
            change = 0;
        }
        this.mana = change;
    }

    public void replenishMana() {
        int change = this.mana + 15;
        if(change > 100) {
            change = 100;
        }
        this.mana = change;
    }

    public void attack(Hero targetHero, int amount) {
        this.spendMana(25);
        targetHero.changeHP(-amount);
    }

    public void ultraAttack(Hero targetHero) {
        this.spendMana(70);
        targetHero.changeHP(-80);
    }

    public void areaAttack(Hero[] heros) {
        this.spendMana(50);
        for (Hero hero : heros) {
            hero.changeHP(-30);
        }
    }

    public String getName() {
        return this.name;
    }
    
    public int getHP() {
        return this.hp;
    }

    public int getMana() {
        return this.mana;
    }
    
}