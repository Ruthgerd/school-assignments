package com.ruthco.app;

public class App {
  
	public static void main(String[] args){
        Skill iceMissile = new Skill("Ice Missile",20,3, "Ice");
        Skill fireball = new Skill("Fire Ball",60,5, "Fire");
        Skill atomicKick = new Skill("Atomic Kick",35,4, "Physical");
        
		fireball.useSkill();

        iceMissile.useSkill();

        atomicKick.useSkill();

        Hero[] heros = new Hero[3];
        heros[0] = new Hero("Batman");
        heros[1] = new Hero("Deadpool");
        heros[2] = new Hero("Samus");

        // ROUND 1
        System.out.println("\nROUND 1");

        heros[2].changeHP(-50);
        heros[1].changeHP(-90);
        heros[0].spendMana(50);

        for (Hero hero : heros) {
            System.out.println(hero.getName() + " has: " + hero.getHP() + "HP, " + hero.getMana() + "MP");
        }

        // ROUND 2
        System.out.println("\nROUND 2");

        heros[1].changeHP(50);
        heros[0].spendMana(60);
        heros[2].changeHP(-40);

        for (Hero hero : heros) {
            System.out.println(hero.getName() + " has: " + hero.getHP() + "HP, " + hero.getMana() + "MP");
        }

        // ROUND 3
        System.out.println("\nROUND 3");

        heros[2].spendMana(100);
        heros[1].changeHP(-80);
        heros[0].changeHP(-80);

        for (Hero hero : heros) {
            System.out.println(hero.getName() + " has: " + hero.getHP() + "HP, " + hero.getMana() + "MP");
        }

        // RESET
        System.out.println("\nROUND 4");
        heros[0] = new Hero("Batman");
        heros[1] = new Hero("Deadpool");
        heros[2] = new Hero("Samus");

        heros[0].attack(heros[2], 50);
        heros[0].ultraAttack(heros[1]);
        heros[0].areaAttack(new Hero[] {heros[1], heros[2]});

        for (Hero hero : heros) {
            System.out.println(hero.getName() + " has: " + hero.getHP() + "HP, " + hero.getMana() + "MP");
        }
	}
}
