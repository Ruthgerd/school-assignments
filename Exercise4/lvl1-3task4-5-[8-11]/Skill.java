package com.ruthco.app;

public class Skill {
    private String name;
    private String type;
    private int damage;
    private int manaCost;

    public Skill(String name, int damage, int manaCost, String type) {
        this.name = name;
        this.type = type;
        this.damage = damage;
        this.manaCost = manaCost;
    }

    public void useSkill() {
        System.out.println(this.name + " causes " + this.damage + " of " + this.type + " damage. (costs " + this.manaCost + " mana)");
    }

    public String getName() {
        return this.name;
    }

    public String getDamage() {
        return this.name;
    }

    public String getManaCost() {
        return this.name;
    }

}