package com.ruthco.app;

public class Car {
    private String carName;
    private double dailyRent;

    public Car(String carName, double d) {
        this.carName = carName;
        this.dailyRent = d;
    }

    public String getName() {
        return this.carName;
    }
    public double getPrice() {
        return this.dailyRent;
    }
    public double getPrice(int days) {
        return this.dailyRent * days;
    }
}