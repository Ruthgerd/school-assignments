package com.ruthco.app;

public class Person {
    private String name;
    private Double money;
    public Person(String name, Double money) {
        this.name = name;
        this.money = money;
    }

    public String getName() {
        return this.name;
    }

    public Double getMoney() {
        return this.money;
    }
}