package com.ruthco.app;

import java.util.Scanner;

public class App {

    public static Person[] doubleArray(Person[] oldArray) {
        Person[] newArray = new Person[oldArray.length*2];
        
        for (int i = 0; i < oldArray.length; i++) {
            newArray[i] = oldArray[i];
        }
        return newArray;
    }
	public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        
        int choice;
        Person[] people = new Person[2];
        int peopleCount = 0;

        do {
            System.out.println();
            System.out.print("Add person(1) or List people(2): ");
            choice = scanner.nextInt();
            scanner.nextLine();

            System.out.println();
            if (choice == 1) {
                System.out.print("Give a name: ");
                String name = scanner.nextLine();

                System.out.print("Give the amount of money: ");
                double money = scanner.nextInt();
                scanner.nextLine();

                if (people.length == peopleCount) {
                    people = doubleArray(people);
                }
                people[peopleCount] = new Person(name, money);
                peopleCount++;

            } else if (choice == 2) {
                System.out.println();
                for (int i = 0; i < peopleCount; i++) {
                    System.out.println("Name: " + people[i].getName() + " money: " + people[i].getMoney());
                }
            }
        } while(choice == 1 || choice == 2);

        scanner.close();
    }
}
