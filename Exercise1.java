import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Scanner input = new Scanner(System.in);

        // Ex1
        System.out.print("\nInput a temp in C*: ");
        float celsius = input.nextFloat();

        float fahrenheit = 9f / 5f * celsius + 32;
        System.out.println("Temp in F*: " + fahrenheit);

        // Ex2
        System.out.print("\nEnter the subtotal: ");
        float subtotal = input.nextFloat();

        System.out.print("Enter the gratuity rate: ");
        float gratuity = input.nextFloat();

        float gratuitymodifier = gratuity / 100f + 1;
        float totalcost = subtotal * gratuitymodifier;
        float gratuitycost = totalcost - subtotal;

        System.out.println("The gratuity is $"+gratuitycost+" and total is $" + totalcost);

        // Ex3
        System.out.print("\nEnter a number between 1000: ");
        int inputNumber = input.nextInt();
        if (inputNumber <= 1000 && inputNumber >= 0) {
            String numberString = Integer.toString(inputNumber);
            int sumOfNumber = 0;
            for (int i = 0; i < numberString.length(); i++) {
                sumOfNumber += Integer.parseInt(numberString.substring(i, i+1));
            }
            System.out.println("The sum of the number is: " + sumOfNumber);
        } else {
            System.out.println("Invalid input");
        }

        // Ex4
        System.out.print("\nEnter the driving distance: ");
        float distance = input.nextFloat();

        System.out.print("Enter miles per gallon: ");
        float milesPerGallon = input.nextFloat();

        System.out.print("Enter price per gallon: ");
        float pricePerGallon = input.nextFloat();

        float costOfDrive = Math.round((distance / milesPerGallon) * pricePerGallon * 100f) / 100f;
        System.out.println("The cost of driving is: $" + costOfDrive);
        
        // Ex5
        System.out.print("\nInput a word: ");
        String someWord = input.next();

        System.out.println("The lenght of the word \"" + someWord + "\" is " + someWord.length() + ",\nand the first letter is: " + someWord.substring(0, 1));

        // Ex6
        System.out.print("\nEnter employee's name: ");
        String employeeName = input.next();

        System.out.print("Enter number of hours worked in a week: ");
        float hoursWorked = input.nextFloat();

        System.out.print("Enter hourly pay rate: ");
        float payRate = input.nextFloat();

        System.out.print("Enter federal tax withholding rate (%): ");
        float fedTax = input.nextFloat() / 100;

        System.out.print("Enter state tax withholding rate (%): ");
        float stateTax = input.nextFloat() / 100;


        float grossPay = payRate * hoursWorked;
        float fedTaxMoney = payRate * hoursWorked * fedTax;
        float stateTaxMoney = payRate * hoursWorked * stateTax;
        float totalDeduction = stateTaxMoney + fedTaxMoney;
        float netPay = grossPay - totalDeduction;
        
        System.out.println("Employee Name: " + employeeName);
        System.out.println("Hours Worked: " + hoursWorked);
        System.out.println("Pay Rate: $" + payRate);
        System.out.println("Gross pay: $" + grossPay);
        System.out.println("Deductions:");
        System.out.println("Federal Withholding ("+ fedTax * 100f+"%): $" + fedTaxMoney);
        System.out.println("State Withholding ("+ stateTax * 100f+"%): $" + stateTaxMoney);
        System.out.println("Total Deduction: $" + totalDeduction);
        System.out.println("Net Pay: $" + netPay);

        input.close();
    }
}