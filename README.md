# School assignments
### This is "chillin behind laptop" grade code not "production" grade,<br>every decision was made on a whim and you wont see the best practices.
If you have any questions feel free to contact me :)   

Exercise 1  
status:  
done, all in a single function, one file.  
https://docs.google.com/document/d/1YF4pzu2bJaDBJS9fKPRqbh36emtBtvlDdqCO_cvzyAM/edit  
  
Exercise 2:  
status:  
done, in seperate functions, one file.  
https://docs.google.com/document/d/1WsdVWeXvvWbokPxYjCOTDKEO1dsMjFnhg7IjbehJ01w/edit  
  
Exercise 3:  
Status:  
not started/skipped might do in future  

Exercise 4:  
Status:  
done, The tasks are split up this time around.  
task 4 has been skipped since it has been done before.  
https://docs.google.com/document/d/1XkAc_qvMNLu44LOiqr7Q_xsIhD-JjNkrwTKzvsLPxPA/edit  
